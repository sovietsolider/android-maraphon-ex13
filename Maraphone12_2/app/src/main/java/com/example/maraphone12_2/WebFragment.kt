package com.example.maraphone12_2

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebBackForwardList
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.activity.addCallback
import androidx.fragment.app.FragmentManager
import androidx.navigation.Navigation

class WebFragment : Fragment() {
    private lateinit var fragmentView: View
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fragmentView =  inflater.inflate(R.layout.fragment_web, container, false)
        val wbView = fragmentView.findViewById<WebView>(R.id.wbView)
        wbView.webViewClient = WebViewClient()
        val sp: SharedPreferences = requireActivity().getPreferences(Context.MODE_PRIVATE)
        val loadedUrl = sp.getString("LAST_URL", "https://www.google.com/")
        wbView.loadUrl(loadedUrl!!)
        if(savedInstanceState != null)
            wbView.restoreState(savedInstanceState)
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner){
            wbView.goBack()
        }

        return fragmentView
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        val wbView = fragmentView.findViewById<WebView>(R.id.wbView)
        wbView.saveState(outState)

    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) = WebFragment()
    }

    private fun saveUrl() {
        val sp= requireActivity().getPreferences(Context.MODE_PRIVATE)
        val editor = sp.edit()
        val wbView = fragmentView.findViewById<WebView>(R.id.wbView)
        editor.putString("LAST_URL", wbView.copyBackForwardList().currentItem?.url)
        editor.commit()
    }

    override fun onDestroy() {
        super.onDestroy()
        saveUrl()
    }


}