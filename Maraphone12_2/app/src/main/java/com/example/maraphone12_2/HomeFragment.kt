package com.example.maraphone12_2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.navigation.ui.AppBarConfiguration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView

class HomeFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val adapter = RecyclerAdapter()
        val listOfStringsForItems = listOf("Двадцать семь", "Это клуб")
        val view =  inflater.inflate(R.layout.fragment_home, container, false)
        activity?.findViewById<BottomNavigationView>(R.id.bottomNavView)?.visibility = View.VISIBLE

        val rcView = view.findViewById<RecyclerView>(R.id.rc_view)
        rcView.layoutManager = LinearLayoutManager(this.context)
        rcView.adapter = adapter

        for(i in listOfStringsForItems) {
            adapter.addItem(RecItem(i))
        }
        return view
    }



    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) = HomeFragment()
    }
}